import logging
import RPi.GPIO as GPIO

class Relay(object):

    def __str__(self):
        return "This object controls a relay using RPI.GPIO package.\n"

    def __init__(self, pin):
        self.PIN = pin
        self.state = False
        GPIO.setmode(GPIO.BCM)
        GPIO.setwarnings(False)
        GPIO.setup(self.PIN, GPIO.OUT, initial=GPIO.LOW)
        # Make sure pin is originally set low
        logging.info("Relay assigned to pin %s setup" % self.PIN)

    def setHigh(self):
        GPIO.output(self.PIN, GPIO.HIGH)
        self.state = True
        logging.info("Relay assigned to pin %s has been set %s" % (self.PIN, self.state))

    def setLow(self):
        GPIO.output(self.PIN, GPIO.LOW)
        self.state = False
        logging.info("Relay assigned to pin %s has been set %s" % (self.PIN, self.state))

    def __del__(self):
        import RPi.GPIO as GPIO
        self.setLow()
        # GPIO.cleanup()




if __name__ == '__main__':
    import time
    from gpio import *
    power  = Relay(POWER_RELAY)
    open = Relay(OPEN_RELAY)
    close = Relay(CLOSE_RELAY)
    power2 = Relay(POWER_RELAY_POWER)
    open2 = Relay(OPEN_RELAY_POWER)
    close2 = Relay(CLOSE_RELAY_POWER)
    for index in range(10):
        power.setHigh()
        power2.setHigh()
        time.sleep(1.0)
        power.setLow()
        power2.setLow()

        open.setHigh()
        open2.setHigh()
        time.sleep(1.0)
        open.setLow()
        open2.setLow()

        close.setHigh()
        close2.setHigh()
        time.sleep(1.0)
        close.setLow()
        close2.setLow()
    del(power)
    del(power2)
    del(open)
    del(open2)
    del(close)
    del(close2)
