import logging
import RPi.GPIO as GPIO
from gpio import *
import time



class DistanceMeter(object):

    def __str__(self):
        return "This class is used to measure the distance using the ultrasonic distance sensor,"

    def __init__(self, mindistance = 0.15, maxdistance = 0.5):
        self.TRIG = TRIG_DISTANCEMETER
        self.ECHO = ECHO_DISTANCEMETER
        self.POWER = POWER_DISTANCEMETER
        GPIO.setmode(GPIO.BCM)
        GPIO.setwarnings(False)
        GPIO.setup(self.TRIG, GPIO.OUT, initial=GPIO.LOW)
        GPIO.setup(self.ECHO, GPIO.IN)
        GPIO.setup(self.POWER, GPIO.OUT, initial=GPIO.LOW)
        logging.info("Ultrasonic distance meter setup!")
        self.distance = 0
        self.timeDelta = 0
        self.powerIsOn = False
        self.minDistance = mindistance
        self.maxDistance = maxdistance
        self.powerOn()

    def powerOn(self):
        """
        Turns on the device
        :return:
        """
        if not self.powerIsOn:
            GPIO.output(self.POWER, GPIO.HIGH)
            GPIO.output(self.TRIG, GPIO.LOW)
            # Waiting For Sensor To Settle
            time.sleep(0.1)
            self.powerIsOn = True
            logging.info("Distance meter turned on")


    def powerOff(self):
        """
        Turns off the device
        :return:
        """
        if self.powerIsOn:
            GPIO.output(self.POWER, GPIO.LOW)
            GPIO.output(self.TRIG, GPIO.LOW)
            self.powerIsOn = False
            logging.info("Distance meter turned off")

    def measureRange(self):
        """
        Measures the current distance
        :return:
        """
        self.powerOn()
        # Trigger and wait shortest possible time
        GPIO.output(self.TRIG, GPIO.HIGH)
        time.sleep(0.00001)
        GPIO.output(self.TRIG, GPIO.LOW)

        # Wait for response
        while GPIO.input(self.ECHO) == 0:
            pulse_start = time.time()

        while GPIO.input(self.ECHO) == 1:
            pulse_end = time.time()

        self.timeDelta = pulse_end - pulse_start
        self.distance = self.timeDelta * 171.5
        return self.distance

    def percentOpen(self):
        """
        Convert the distance into percent according with the max and min specifications in the constructor
        :return:
        """
        return (self.distance - self.minDistance) / (self.maxDistance - self.minDistance) * 100.

    def __del__(self):
        GPIO.output(self.TRIG, GPIO.LOW)
        GPIO.output(self.POWER, GPIO.LOW)
        # GPIO.cleanup()

if __name__ == "__main__":
    dm = DistanceMeter()
    dm.powerOn()
    for index in range(100):
        print(dm.measureRange())
        time.sleep(0.2)
    dm.powerOff()
    del(dm)
