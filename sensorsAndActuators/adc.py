import time

# Import SPI library (for hardware SPI) and MCP3008 library.
import Adafruit_GPIO.SPI as SPI
import Adafruit_MCP3008


SPI_PORT   = 0
SPI_DEVICE = 0

class mpc3008(object):
    """
    This is the class wrapper for the ADC MPC3008 connected to the hardware SPI ports.
    """
    def __init__(self):
        self.mcp = Adafruit_MCP3008.MCP3008(spi=SPI.SpiDev(SPI_PORT, SPI_DEVICE))

    def read(self, group):
        # Grab the difference between channel 0 and 1 (i.e. channel 0 minus 1).
        # Note you can specify any value in 0-7 to grab other differences:
        #  - 0: Return channel 0 minus channel 1
        #  - 1: Return channel 1 minus channel 0
        #  - 2: Return channel 2 minus channel 3
        #  - 3: Return channel 3 minus channel 2
        #  - 4: Return channel 4 minus channel 5
        #  - 5: Return channel 5 minus channel 4
        #  - 6: Return channel 6 minus channel 7
        #  - 7: Return channel 7 minus channel 6
        return self.mcp.read_adc_difference(group)