import os
import glob
import time
import RPi.GPIO as GPIO
from gpio import *
import subprocess


class Temperature(object):

    def __init__(self, powerPin, serialNumber):
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(powerPin, GPIO.OUT, initial=GPIO.LOW)
        self.power_pin = powerPin
        self.serialNumber = serialNumber
        self.power = False

        # This relies on the fact that only one temperature sensor
        # is switched on at a time.
        self.turnOn()
        # These should be moved into some docker building file.

        os.system('modprobe w1-gpio')
        os.system('modprobe w1-therm')

        deviceFolder = '/sys/bus/w1/devices/28-' + str(serialNumber)
        # device_folder = glob.glob(base_dir + '*')[0]
        self.device_file = deviceFolder + '/w1_slave'

        # Should default be to turn off device?
        # self.turnOff()


    def read_temp_raw(self):
        f = open(self.device_file, 'r')
        lines = f.readlines()
        f.close()
        return lines

    def read_temp(self):
        lines = self.read_temp_raw()
        while lines[0].strip()[-3:] != 'YES':
            time.sleep(0.2)
            lines = self.read_temp_raw()
        equals_pos = lines[1].find('t=')
        if equals_pos != -1:
            temp_string = lines[1][equals_pos+2:]
            temp_c = float(temp_string) / 1000.0
            # temp_f = temp_c * 9.0 / 5.0 + 32.0
            return temp_c#, temp_f

    def turnOn(self):
        GPIO.output(self.power_pin, GPIO.HIGH)
        time.sleep(5.)
        print("Turned on tempsensor %s" % (self.serialNumber))
        self.power = True

    def turnOff(self):
        self.power = False
        GPIO.output(self.power_pin, GPIO.LOW)

    def Temp(self):
        if not self.power:
            self.turnOn()
        return self.read_temp()

if __name__ == "__main__":
    # GPIO.cleanup()
    temp1 = Temperature(TEMP_POWER_1, TEMP_SERIAL_1)
    temp2 = Temperature(TEMP_POWER_2, TEMP_SERIAL_2)

    for index in range(100):
        print("%s reads %s" % (temp1.serialNumber, temp1.Temp()))
        print("%s reads %s" % (temp2.serialNumber, temp2.Temp()))
