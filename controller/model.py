import numpy as np
import scipy.linalg

class Model(object):

    def __init__(self, Ts = 1.):
        self.order = 2
        self.A = np.eye(self.order, k=1)
        #TODO Find a reasonable value for the speed of the motor perhaps by calibration step
        self.B = np.array([[0], [1.]])
        self.C = np.zeros_like(self.B)
        self.C[0] = 1.
        self.Ts = Ts
        self.stateNoise = 0.1 * np.eye(self.order)
        self.observationNoise = 1e-2
        self.discretize(self.Ts)

    def discretize(self, Ts):
        self.Ad = scipy.linalg.expm(self.A * Ts)
        self.Bd = np.dot(np.linalg.pinv(self.A), np.dot(self.Ad - np.eye(self.order), self.B))
        self.Q = np.outer(self.Bd, self.Bd) * self.stateNoise