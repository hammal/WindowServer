import time
import numpy as np

from sensorsAndActuators.distancemeter import DistanceMeter
from .model import Model
from .motor import Motor

MAX_TIME_ON = 30


class Controller(object):
    def __str__(self):
        # Measure distance
        distance = self.distanceMeter.measureRange()
        # Check what motor is doing
        if self.motor.power.state:
            if self.motor.open.state:
                direction = "opening"
            else:
                direction = "closing"
            motorState = "on and %s" % direction
        else:
            motorState = "off"

        status = "This object controls the window to a given position:\nCurrently the window is opened to %s percent\nThe motor is %s" % (
        distance, motorState)
        return status

    def __init__(self):
        self.motor = Motor()
        self.distanceMeter = DistanceMeter()
        self.model = Model()
        self.stateEstimatePrior = np.zeros_like(self.model.B)
        self.covarianceEstimatePrior = np.zeros_like(self.model.Q)
        self.stateEstimatePosterior = np.zeros_like(self.model.B)
        # Initalize with large covariance
        self.covarianceEstimatePosterior = np.eye(self.model.order) * 1e5
        self.epsilon = 3e-3/2.

    def kalmanPredict(self, control):
        """
        This is the prediction step in the Kalman filter see
        https://en.wikipedia.org/wiki/Kalman_filter

        :param control:
        the control desicion
        :return:
        """
        self.covarianceEstimatePrior = np.dot(self.model.Ad, np.dot(self.covarianceEstimatePosterior,
                                                                    self.model.Ad.transpose())) + self.model.Q
        self.stateEstimatePrior = np.dot(self.model.Ad, self.stateEstimatePosterior) + self.model.Bd * control

    def kalmanUpdate(self):
        """
        This is the update step in the Kalman filter see
        https://en.wikipedia.org/wiki/Kalman_filter
        :return:
        """
        residual = self.distanceMeter.measureRange() - np.dot(self.model.C, self.stateEstimatePrior)
        residualCovariance = np.dot(self.model.C.transpose(),
                                    np.dot(self.covarianceEstimatePrior, self.model.C)) + self.model.observationNoise
        kalmanGain = np.dot(self.covarianceEstimatePrior, np.dot(self.model.C, np.linalg.inv(residualCovariance)))
        self.stateEstimatePosterior = self.covarianceEstimatePosterior + kalmanGain * residual
        self.covarianceEstimatePosterior = np.dot(np.eye(self.model.order) - np.outer(kalmanGain, self.model.C),
                                                  self.covarianceEstimatePrior)

    def controlLoop(self, target_position):
        """
        Here we need a clever way of deciding whether the control should stop using the kalmanPredict and Update...
        :return:
        """
        delta = target_position - np.dot(self.model.C, self.stateEstimatePosterior)
        while np.abs(delta) > self.epsilon:
            tstart = time.time()
            if delta > 0:
                self.motor.open()
            else:
                self.motor.close()
            self.kalmanPredict()
            self.kalmanUpdate()
            delta = target_position - np.dot(self.model.C, self.stateEstimatePosterior)
            self.model.Ts = 0.95 * self.model.Ts + 0.05 * (time.time() - tstart)
        self.motor.stop()


    def calibrate(self):
        """
        Here the model speed parameter and observation noise parameters should be set

        For motor speed measurement
        1) Drive window to full close
        2) Start to open window and simultaneously measure distance and time jointly

        For loop time
        time the execution speed of the controlLoop and update the model Ts and discretize.

        For measurment noise:
        Put the window at given position and compute sample variance.
        :return:
        """
        pass


    def goto(self, position):
        return True

    def turnOff(self):
        self.motor.powerOff()
        self.distanceMeter.powerOff()


def requestPosition():
    """
    This function handles the interaction with the webserver
    :return:
    """
    return 0.5


if __name__ == "__main__":
    controller = Controller()
    exitConditions = False
    startingTime = time.time()
    while not exitConditions:
        # Check for new position
        position = requestPosition()
        # Is the controller already there?
        alreadyThere = controller.goto(position)
        # Check if exceeded max tim
        timeLimitExceed = (startingTime - time.time()) > MAX_TIME_ON
        if alreadyThere or timeLimitExceed:
            controller.turnOff()
            exitConditions = True
        time.sleep(1.)
