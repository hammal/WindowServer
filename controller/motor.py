import logging

from sensorsAndActuators.relay import Relay
from sensorsAndActuators.gpio import *

class Motor(object):

    def __str__(self):
        return "This object controls the motor by driving three relays through the GPIO pins"

    def __init__(self):
        self.power = Relay(POWER_RELAY)
        self.open = Relay(OPEN_RELAY)
        self.close = Relay(CLOSE_RELAY)
        logging.info("The motor has been initialised")

    def powerOn(self):
        self.power.setHigh()

    def powerOff(self):
        self.power.setLow()

    def startOpen(self):
        self.close.setLow()
        if not self.close.state:
            self.open.setHigh()
            self.powerOn()
            if self.power.state and self.open.state:
                logging.info("Motor starting to open window")
            else:
                logging.error("The power relay unable to turn on")
        else:
            logging.error("The 'close' relay won't turn off")

    def startClose(self):
        self.open.setLow()
        if not self.open.state:
            self.close.setHigh()
            self.powerOn()
            if self.power.state and self.close.state:
                logging.info("Motor starting to close window")
            else:
                logging.error("The power relay unable to turn on")
        else:
            logging.error("The 'open' relay won't turn off")

    def stop(self):
        self.powerOff()
        self.open.setLow()
        self.close.setLow()
        if self.power.state or self.open.state or self.close.state:
            logging.error("Can't turn off relays!!!")
